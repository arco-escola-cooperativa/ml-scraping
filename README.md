# Scraping de Mercado Livre para a Arco Escola-Cooperativa

## Fazendo o setup

1. Siga [essas](https://docs.gspread.org/en/latest/oauth2.html#for-bots-using-service-account)
   instruções para habilitar a comunicação com google sheets
   e google sheets.
2. Siga [essas](https://pythonhosted.org/PyDrive/quickstart.html)
   instruções para habilitar a comunicação com google drive.
3. Crie uma tabela com ao menos duas abas: uma para dados e uma para
   registro de execução. Atualize o arquivo `defs.py` com os ids (da
   tabela e das abas)
4. Crie duas pastas, uma para os PDFs e outra para os XMLs. Atualize o
   arquivo `defs.py` com os ids dessas pastas
5. Para o envio de emails, siga
   [esses](https://support.google.com/accounts/answer/185833) passos
   para criar uma senha de aplicativo no gmail. Registre essa senha e o
   email do usuário em `segredos/email.py`
