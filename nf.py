from nfelib.nfe.bindings.v4_0.proc_nfe_v4_00 import NfeProc

def parse(path, extra_vals):
    try:
        nfe = NfeProc.from_path(path)
    except UnicodeDecodeError:
        with open(path, "r", encoding="ISO-8859-1") as f:
            nfe = NfeProc.from_xml(f.read())

    nf_id = nfe.NFe.infNFe.Id
    valor_total = nfe.NFe.infNFe.total.ICMSTot.vNF

    lines = []
    for det in nfe.NFe.infNFe.det:
        lines.append({
            "id da nota fiscal": nf_id,
            "título": det.prod.xProd,
            "unidade": det.prod.uCom,
            "quantidade": float(det.prod.qCom),
            "valor unitário": float(det.prod.vUnCom),
            "valor total": float(det.prod.vProd),
            "número do produto": int(det.nItem),
            **extra_vals,
        })

    return nf_id, valor_total, lines


if __name__ == "__main__":
    nf_id, valor_total, lines = parse(
        "/home/jms/.mlscraping/nfs/2023.07.17-Cola Branca Líquida Cascorez "
        "Cascola Extra Adesivo Pva 500g", {}
    )

    print(nf_id)
    print(valor_total)
    print("  ", end="")
    print(*lines, sep="\n  ")
