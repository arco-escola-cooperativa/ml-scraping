import datetime

# Google drive
PDFS_FOLDER_ID = "1H1eDjk6SkRzcbHtc___OSJ-q1Bz7rpfm"
XMLS_FOLDER_ID = "15Ohg_NvfUZv_WRF-_AgEfZY5gkV5ZklI"

# Google sheets
TABLE_ID = "1pwlN-WYnT04S9PHsoy8MQ06HFvm6iJWuM_iQAVHcmco"
DATA_SHEET_ID = "0"
LOG_SHEET_ID = "1612826528"
SERVICE_JSON = "segredos/scraping-ml-416513-1b2f2e412b96.json"

# Data limite: se for pra trás dessa data, não analisa
DATE_LIMIT = datetime.datetime(2024, 2, 1)

# Headless: executa de maneira "invisível", sem aparecer tela
# (não adianta se precisar fazer login)
HEADLESS = True

PROFILE = "arco"

EMAIL_RECEIVERS = [
    "joaoseckler@gmail.com",
    # "leo@arco.coop.br",
    # "tesouraria@arco.coop.br",
    "automatizacao@arco.coop.br"
]
