#!/usr/bin/env python3

from pathlib import Path
import re
from time import sleep
from argparse import ArgumentParser
import datetime
from urllib import parse
from xdg import BaseDirectory

from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

import nf
import table
import files
import send_email
from defs import DATE_LIMIT, HEADLESS, PROFILE


MONTHS = {
    "janeiro": 1,
    "fevereiro": 2,
    "março": 3,
    "abril": 4,
    "maio": 5,
    "junho": 6,
    "julho": 7,
    "agosto": 8,
    "setembro": 9,
    "outubro": 10,
    "novembro": 11,
    "dezembro": 12,
}

THIS_YEAR = datetime.date.today().year

ID_PATTERN = re.compile(r".*my_purchases\/(\d+)\/.*$")
PAGE_SEARCH_PATTERN = re.compile(r"page=(\d+)")
PAGE_SUB_PATTERN = re.compile(r"^(.*)(page=)(\d+)(.*)$")

class NoNFException(NoSuchElementException):
    pass


class MLScraper():
    def __init__(self, headless=True):
        self.options = self.parse_options()

        self.base_folder = Path(
            BaseDirectory.save_data_path("mlscraping")
        ) / PROFILE
        self.download_folder = self.get_download_folder()
        self.profile_folder = self.get_profile_folder()
        self.headless = False if headless == False else HEADLESS

        self.driver = self.driver_init()
        self.log_messages = []

    def log(self, msg):
        self.log_messages.append(("LOG", msg))
        print("LOG:", msg)

    def error(self, msg):
        if "Stacktrace" in msg:
            msg = msg[:msg.index("Stacktrace")].strip()

        self.log_messages.append(("ERROR", msg))
        print("ERROR:", msg)

    def parse_options(self):
        parser = ArgumentParser(
            description="Ler as notas fiscais do mercado livre!"
        )

        parser.add_argument(
            "-w",
            "--wait-time",
            default=7.0,
            type=float,
            help="Tempo máximo de espera para uma página carregar (segundos)"
        )

        parser.add_argument(
            "-d",
            "--download-folder",
            default=None,
            help="Pasta onde colocar os downloads"
        )

        options = parser.parse_args()

        return options

    def get_download_folder(self):
        if self.options.download_folder is None:
            folder = self.base_folder / "downloads"
            folder.mkdir(parents=True, exist_ok=True)
            return str(folder)

        return self.options.download_folder


    def get_profile_folder(self):
        folder = self.base_folder / "profile"
        folder.mkdir(parents=True, exist_ok=True)
        return str(folder)

    def driver_init(self):
        profile_folder = self.profile_folder
        download_folder = self.download_folder

        prefs = {
            "download.default_directory": download_folder,
            "download.directory_upgrade": True,
        }

        options = webdriver.ChromeOptions()
        options.add_argument(f"user-data-dir={profile_folder}")
        if self.headless:
            options.add_argument("--headless=new")
        options.add_experimental_option("prefs", prefs)

        driver = webdriver.Chrome(options=options)
        driver.implicitly_wait(self.options.wait_time)

        return driver

    def has_one_pdf(self):
        driver = self.driver

        try:
            info = driver.find_element(
                By.XPATH,
                '//*[text()="Informações da compra"]/../..'
            )
        except NoSuchElementException:
            raise NoNFException

        if " faturas disponíveis" in info.text:
            return False
        return True

    def get_date(self):
        text = self.driver.find_element(
            By.XPATH,
            '//span[text()="Detalhe da compra"]/../..'
        ).text

        date_string = text.split("\n")[1].split("|")[0].strip()
        date_els = date_string.split("de ")

        day = int(date_els[0])
        month = MONTHS[date_els[1].lower().strip()]
        year = int(date_els[2]) if len(date_els) > 2 else THIS_YEAR

        return datetime.datetime(year, month, day)

    def toggle_tabs(self, first=False):
        for tab in self.driver.window_handles:
            if first or tab != self.driver.current_window_handle:
                self.driver.switch_to.window(tab)
                break

    def close_if_not_last_tab(self):
        if len(list(self.driver.window_handles)) > 1:
            self.driver.close()

    def handle_downloaded_file(self, date):
        sleep_time = 0.5
        count = 60

        folder = Path(self.download_folder)

        sleep(1)

        while any(
            fn.suffix in (".crdownload", ".tmp")
            for fn in folder.glob("*")
        ) or len(list(folder.glob("*"))) == 0:
            sleep(sleep_time)
            count -= 1

            if count < 0:
                return False

        target_folder = folder.parent / "nfs"
        target_folder.mkdir(exist_ok=True, parents=True)

        current = next(folder.glob("*"))
        target = target_folder / self.nf_fname(date, current.name)

        current.rename(target)
        return target

    def extrai_id(self, url):
        mo = ID_PATTERN.search(url)

        if not mo:
            raise ValueError(f"Não achei o id desse url: {url}")

        return mo.group(1)

    def nf_fname(self, date, name):
        return date.strftime("%Y.%m.%d") + "-" + name

    def clear_download_folder(self):
        for file in Path(self.download_folder).glob('*.crdownload'):
            file.unlink(missing_ok=True)

    def add_url_params(self, url, params):
        # https://stackoverflow.com/a/25580545

        url = parse.unquote(url)
        parsed_url = parse.urlparse(url)
        get_args = parsed_url.query

        parsed_get_args = dict(parse.parse_qsl(get_args))
        parsed_get_args.update(params)

        encoded_get_args = parse.urlencode(parsed_get_args, doseq=True)
        new_url = parse.ParseResult(
            parsed_url.scheme, parsed_url.netloc, parsed_url.path,
            parsed_url.params, encoded_get_args, parsed_url.fragment
        ).geturl()

        return new_url

    def detect_empty_page(self, new_page):
        """If new page (the one used in url parameter) is greater thand
        current page displayed on screen, than"""

        classname = "andes-pagination__button--current"

        try:
            current_button = self.driver.find_element(By.CLASS_NAME, classname)
        except NoSuchElementException:
            self.error(
                f"Não consegui detectar o número da página: {self.driver.current_url}"
            )

        try:
            current_page = int(current_button.text.strip())
            new_page = int(new_page)
        except ValueError:
            self.error(
                f"Não consegui detectar o número da página: {self.driver.current_url}"
            )
            return False

        return current_page < new_page

    def get_current_page(self):
        url = self.driver.current_url
        mo = PAGE_SEARCH_PATTERN.search(url)

        return int(mo.group(1)) if mo else 1


    def go_to_next_page(self):
        url = self.driver.current_url
        next_page = self.get_current_page() + 1

        new_url = self.add_url_params(url, {"page": next_page})
        self.driver.get(new_url)

        return self.detect_empty_page(next_page)

    def unique_urls_and_ids(self, compras, used_ids, pending_ids):
        """
        returns unique urls with unique ids, which haven't been used or
        that have been used but are pending
        """
        hrefs = map(lambda c: c.get_attribute("href"), compras)

        id_urls = {href: self.extrai_id(href) for href in hrefs}

        return [
            (url, compra_id)
            for url, compra_id
            in id_urls.items()
            if (compra_id not in used_ids or compra_id in pending_ids)
        ]

    def detect_canceled(self):
        try:
            self.driver.find_element(
                By.XPATH,
                "//*["
                "contains(translate(text(), 'C', 'c'), 'cancelad') "
                "]"
            )
        except NoSuchElementException:
            return False

        return True

    def detect_reimbursed(self):
        try:
            self.driver.find_element(
                By.XPATH,
                "//*["
                "contains(translate(text(), 'R', 'r'), 'reembols') "
                "]"
            )
        except NoSuchElementException:
            return False

        return True

    def detect_pending(self):
        try:
            self.driver.find_element(
                By.XPATH,
                "//*["
                "contains(translate(text(), 'P', 'p'), 'pagamento pendente') "
                "or "
                "contains(translate(text(), 'P', 'p'), 'pendente de pagamento') "
                "]"
            )
        except NoSuchElementException:
            return False

        return True

    def close_tab_after(func):
        def wrapper(self, *args, **kwargs):
            ret = func(self, *args, **kwargs)
            self.close_if_not_last_tab()
            self.toggle_tabs(first=True)
            sleep(1)

            return ret

        return wrapper

    def remove_implicit_wait(self):
        self.driver.implicitly_wait(0)

    def restore_implicit_wait(self):
        self.driver.implicitly_wait(self.options.wait_time)

    @close_tab_after
    def handle_compra(self, url, was_pending):
        driver = self.driver

        nf_xml_paths = set()

        driver.execute_script('''window.open("", "_blank")''')
        self.toggle_tabs()

        driver.get(url)
        # driver.execute_script("document.body.style.zoom='25%'")
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        date = self.get_date()

        self.remove_implicit_wait()
        pending = self.detect_pending()
        canceled = self.detect_canceled()
        reimbursed = self.detect_reimbursed()

        status = (
            (reimbursed and "reembolsado")
            or (pending and "pendente")
            or (canceled and "cancelado")
            or "realizado"
        )

        if was_pending or status == "reembolsado":
            return [], date, status

        if date < DATE_LIMIT:
            date_fmt = date.strftime("%d/%m/%y")
            self.log(
                f"Cheguei numa compra mais antiga ({date_fmt}) do que o "
                "limite. Encerrando..."
            )

            return [], date, status

        self.restore_implicit_wait()

        if not self.has_one_pdf():
            faturas = driver.find_element(
                By.XPATH,
                '//*[text()="Mostrar faturas"]'
            )
            faturas.click()

        self.restore_implicit_wait()
        download_buttons = driver.find_elements(
            By.XPATH,
            '//*[text()="Baixar em PDF"]'
        )
        download_buttons += driver.find_elements(
            By.XPATH,
            '//*[text()="Baixar em XML"]'
        )

        for btn in download_buttons:
            # move than click
            webdriver.ActionChains(driver).move_to_element(btn).perform()
            btn.click()
            res = self.handle_downloaded_file(date)
            sleep(2)
            if not res:
                self.error(f"Não consegui baixar uma nota fiscal daqui: {url}")
                self.clear_download_folder()
            else:
                nf_xml_paths.add(
                    res.parent / (res.name.removesuffix(".pdf"))
                )

        self.restore_implicit_wait()
        return nf_xml_paths, date, status

    def handle_page(
        self,
        info,
        used_ids,
        pending_ids,
        no_longer_pending,
        used_nf_ids
    ):
        driver = self.driver

        logged_in = False

        while not logged_in:
            compras = driver.find_elements(
                By.XPATH,
                '//span[text()="Ver compra"]/ancestor::a'
            )

            if compras:
                logged_in = True
            else:
                ans = input(
                    "Parece que não estamos logados ou tem algo de "
                    "estranho na tela. Faça login e depois aperte "
                    "enter aqui. Ou aperte 'q' para encerrar a raspagem "
                    "e salvar os dados: "
                )

                if ans == "q":
                    return False

        date = datetime.datetime.now()
        urls_and_ids = self.unique_urls_and_ids(
            compras,
            used_ids,
            pending_ids
        )
        n = len(urls_and_ids)

        for i, (url, compra_id) in enumerate(urls_and_ids):
            print(f"    Entrando em nova compra... ({i + 1}/{n})")
            try:
                was_pending = compra_id in pending_ids

                nf_xml_paths, date, status = self.handle_compra(
                    url, was_pending
                )

                if status == "reembolsado":
                    continue

                if was_pending and status != "pendente":
                    no_longer_pending[compra_id] = status

                if was_pending:
                    continue

                if date < DATE_LIMIT:
                    break

                for path in nf_xml_paths:
                    nf_id, valor_total, lines = nf.parse(path, {
                        "id da compra": compra_id,
                        "link da compra": url,
                        "data": date,
                        "xml_path": path,
                        "status": status,
                    })

                    if nf_id not in used_nf_ids:
                        used_nf_ids.add(nf_id)
                        info += lines

            except NoNFException as e:
                self.error(f"A seguinte compra não tem NF: {url}")
                self.close_if_not_last_tab()
                self.toggle_tabs(first=True)

            except Exception as e:
                self.error(
                    f"Erro ao pegar a seguinte compra: {url}\n"
                    + f"Mensagem de erro:\n\n{repr(e)}"
                    + (
                        f"\n{e}" if isinstance(e, NoSuchElementException)
                        else ""
                    )
                )
                self.close_if_not_last_tab()
                self.toggle_tabs(first=True)

        if not urls_and_ids or date < DATE_LIMIT:
            return True


    def scrape(self):
        driver = self.driver
        driver.get("https://myaccount.mercadolivre.com.br/my_purchases/list")

        info = []
        used_ids, pending_ids = table.get_used_ids()
        used_nf_ids = set()
        no_longer_pending = {}

        nothing_to_do = False
        empty_page = False

        while not nothing_to_do and not empty_page:
            current_page = self.get_current_page()
            print(f"Entrando na página {current_page}...")

            nothing_to_do = self.handle_page(
                info,
                used_ids,
                pending_ids,
                no_longer_pending,
                used_nf_ids
            )
            empty_page = self.go_to_next_page()

        if info:
            df = table.df_init(info)
            df, n = files.upload_nfs(df)
            table.upload(df)

            self.log(f"Fiz o upload de {n} notas fiscais")
            self.log(f"Registrei a compra de {len(df.index)} produtos")

        if no_longer_pending:
            items = table.update_no_longer_pending(no_longer_pending)
            self.log(
                f"Atualizei o status de {len(no_longer_pending)} compras "
                f"({items} items) que estavam pendentes"
            )

        if not info and not no_longer_pending:
            self.log("Nenhuma nova nota ou atualização para fazer upload")

        table.log(self.log_messages)
        send_email.send_log(self.log_messages)

    def browser(self, ask=True):
        self.driver.get("https://myaccount.mercadolivre.com.br/my_purchases/list")
        if ask:
            input("Enter para encerrar: ")


if __name__ == "__main__":
    scraper = MLScraper()
    scraper.scrape()
