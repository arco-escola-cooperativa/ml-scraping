import datetime
import pandas as pd
import numpy as np

import gsheet
from defs import TABLE_ID, DATA_SHEET_ID, LOG_SHEET_ID

def remove_duplicates(df):
    original = gsheet.fetch(TABLE_ID, DATA_SHEET_ID)
    original = original[original.iloc[:,0].notnull()]

    if original.empty:
        return df

    original = original[["id da nota fiscal", "número do produto"]]

    df = df.merge(original, how="left", indicator=True, on=[
        "id da nota fiscal",
        "número do produto",
    ])

    df = df[df["_merge"] == "left_only"]
    df = df.drop(columns = "_merge")

    return df

def df_init(info):
    df = pd.DataFrame(info)
    df = remove_duplicates(df)

    return df

def upload(df):
    df = df[[
        "id da compra",
        "id da nota fiscal",
        "número do produto",
        "data",
        "título",
        "unidade",
        "quantidade",
        "valor unitário",
        "valor total",
        "status",
        "link da compra",
        "NF no drive (PDF)",
        "NF no drive (XML)",
    ]]

    df["id da compra"] = df["id da compra"].apply(
        lambda x: f"{x:.0f}" if type(x) == float else x
    )
    df = df.sort_values(by="data", ascending=True)
    df["data"] = df["data"].dt.strftime("%d/%m/%y")

    if not df.empty:
        gsheet.append(df, TABLE_ID, DATA_SHEET_ID)

def get_used_ids():
    df = gsheet.fetch(TABLE_ID, DATA_SHEET_ID)

    all_ids = set(map(str, map(int,
        pd.unique(df["id da compra"].dropna())
    )))

    df = df[df["status"] == "pendente"]

    pending_ids = set(map(str, map(int,
        pd.unique(df["id da compra"].dropna())
    )))

    return all_ids, pending_ids


def update_no_longer_pending(statuses):
    worksheet = gsheet.get_worksheet(TABLE_ID, DATA_SHEET_ID)
    df = gsheet.fetch(TABLE_ID, DATA_SHEET_ID)

    df["id da compra"] = df["id da compra"].apply(lambda x: f"{x:.0f}")

    count = 0

    def update_row(row):

        if row["id da compra"] in statuses:
            nonlocal count

            count += 1
            return statuses[row["id da compra"]]

        if pd.isnull(row["status"]):
            return ""

        return row["status"]

    count = 0
    df["status"] = df.apply(update_row, axis=1)

    column = np.array(df["status"])
    column = column.reshape((column.shape[0], 1))
    column = list(map(list, column))
    worksheet.update(range_name="J2:J", values=column)

    return count

def log(msgs):
    now = datetime.datetime.now()

    df = pd.DataFrame(msgs, columns=["tipo", "mensagem"])
    df["timestamp"] = now
    df = df[["timestamp", "tipo", "mensagem"]]

    gsheet.append(df, TABLE_ID, LOG_SHEET_ID)
