import smtplib
import datetime
from email.message import EmailMessage
from email.utils import formataddr
from segredos.email import USER, PASSWORD, SENDER_NAME, SENDER_EMAIL
from defs import EMAIL_RECEIVERS, TABLE_ID

def login():
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    server.login(USER, PASSWORD)

    return server

def send_email(receivers, subject, body):
    msg = EmailMessage()
    msg.set_content(body)

    msg["Subject"] = subject
    msg["From"] = formataddr((SENDER_NAME, SENDER_EMAIL))
    msg["To"] = receivers

    server = login()
    server.send_message(msg)
    server.close()

def send_log(log):
    joined = "\n".join(map(" ".join, log))
    url = f"https://docs.google.com/spreadsheets/d/{TABLE_ID}"

    date_short = datetime.datetime.now().strftime("%d/%m/%y")
    date_long = datetime.datetime.now()

    subject = f"Raspagem ML - Resultado de {date_short}"
    body = f"""
Eu terminei de varrer as compras do mercado livre em {date_long}. Seguem os resultados:

-----------------------------
{joined}
-----------------------------

A planilha com os dados está disponível aqui: {url}

Abraços,
Robozinho da Arco
"""

    send_email(EMAIL_RECEIVERS, subject, body)
