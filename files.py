import pandas as pd
from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive
from oauth2client.service_account import ServiceAccountCredentials
import sys

from defs import PDFS_FOLDER_ID, XMLS_FOLDER_ID, SERVICE_JSON

ANSI_3_BACK = "\x1b[3D"

def upload_file(drive, path, is_xml=True):
    if not path.is_file():
        return ""

    file = drive.CreateFile({
        "title": path.name,
        "parents": [{"id": (
            XMLS_FOLDER_ID if is_xml else PDFS_FOLDER_ID
        )}]
    })

    file.SetContentFile(str(path.resolve()))

    file.Upload()
    path.unlink(missing_ok=True)

    return file["alternateLink"]

def xml2pdf_path(xml_path):
    return xml_path.parent / (xml_path.name + ".pdf")

def authenticate():
    gauth = GoogleAuth(settings={
        "client_config_backend": "service",
        "service_config": {
            "client_json_file_path": SERVICE_JSON,
        }
    })
    gauth.ServiceAuth()
    drive = GoogleDrive(gauth)

    return drive

def upload_nfs(df):
    paths = pd.unique(df["xml_path"])
    drive = authenticate()

    paths2urls = {}
    n = len(paths)

    for i, xml_path in enumerate(paths):
        print(f"fazendo upload do arquivo {i + 1}/{n} - ...", end="")
        sys.stdout.flush()
        pdf_path = xml2pdf_path(xml_path)

        xml_link = upload_file(drive, xml_path)
        print(f"{ANSI_3_BACK}XML: {'ok' if xml_link else 'nao tinha'}, ...", end= "")
        sys.stdout.flush()

        pdf_link = upload_file(drive, pdf_path, is_xml=False)
        print(f"{ANSI_3_BACK}PDF: {'ok' if pdf_link else 'nao tinha'}")

        key = str(xml_path)
        original = paths2urls.get(key)

        paths2urls[key] = [
            original[0] if original and original[0] else xml_link,
            original[1] if original and original[1] else pdf_link,
        ]

    df[["NF no drive (XML)", "NF no drive (PDF)"]] = df.apply(
        lambda row: pd.Series(paths2urls[str(row["xml_path"])]),
        axis=1
    )

    df = df.drop(columns="xml_path")

    return df, len(paths)
