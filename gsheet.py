import pandas as pd
import gspread
from gspread_dataframe import get_as_dataframe, set_with_dataframe

from defs import SERVICE_JSON

def get_worksheet(ss_id, ws_id=None):
    gc = gspread.service_account(
        filename=SERVICE_JSON
    )
    ss = gc.open_by_key(ss_id)

    if ws_id:
        return ss.get_worksheet_by_id(ws_id)

    return ss.get_worksheet(0)

def fetch(ss_id: str, ws_id: str | None = None, **kwargs):
    worksheet = get_worksheet(ss_id, ws_id)
    return get_as_dataframe(worksheet, **kwargs)

def upload(dataframe: pd.DataFrame, ss_id: str, ws_id: str | None = None):
    worksheet = get_worksheet(ss_id, ws_id)
    set_with_dataframe(worksheet, dataframe)

def first_empty_row(worksheet, col=1):
    column = worksheet.col_values(col)

    if len(column) == 0:
        return 0

    last = max(
        enumerate(column),
        key=lambda x: -1 if x[1] is None else x[0]
    )[0]

    return last + 1

def append(dataframe: pd.DataFrame, ss_id: str, ws_id: str | None = None):
    worksheet = get_worksheet(ss_id, ws_id)

    row = first_empty_row(worksheet)

    if row <= 1:
        set_with_dataframe(worksheet, dataframe)

    else:
        set_with_dataframe(
            worksheet,
            dataframe,
            include_index=False,
            include_column_header=False,
            row=row + 1,
            resize=False
        )
